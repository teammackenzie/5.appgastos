import React, { Component } from 'react';
import Header from './Header';
import Footer from './Footer';
import Gastos from './Gastos';
import Ingreso from './Ingreso';

export default class Main extends Component {
	state = {
		gastos: [],
		totalGastos: 0,
		cantidadGastos: 0
	};

	addGasto = gasto => {
		const gastos = [...this.state.gastos, gasto];
		const totalGastos = gastos.reduce((total, gasto) => (total += gasto), 0);
		const cantidadGastos = gastos.length;
		this.setState({ gastos, totalGastos, cantidadGastos });
	};

	render() {
		const { gastos, totalGastos, cantidadGastos } = this.state;
		return (
			<div className="container">
				<Header titulo="App Gastos" year={new Date().getFullYear()} />
				<div className="row">
					<Gastos gastos={gastos} totalGastos={totalGastos} cantidadGastos={cantidadGastos} />
					<Ingreso addGasto={this.addGasto} />
				</div>

				<Footer company="Giovanny Vargas" />
			</div>
		);
	}
}
