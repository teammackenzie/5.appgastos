import React from 'react';
import BadgeGastos from './BadgeGastos';
import Gasto from './Gasto';
import TotalGastos from './TotalGastos';

export default function Gastos({ gastos, totalGastos, cantidadGastos }) {
	return (
		<div className="col-md-4 order-md-2 mb-4">
			<BadgeGastos cantidadGastos={cantidadGastos} />
			<ul className="list-group mb-3">{gastos.map((gasto, index) => <Gasto key={index} gasto={gasto} />)}</ul>
			<TotalGastos totalGastos={totalGastos} />
		</div>
	);
}
