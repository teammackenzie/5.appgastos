import React, { Component } from 'react';
import { round } from '../Helper';

export default class Ingreso extends Component {
	state = {
		gasto: 0
	};

	enviarGasto = () => {
		this.props.addGasto(this.state.gasto);
	};

	capturarGasto = event => {
		this.setState({
			gasto: round(event.target.value, 0)
		});
	};

	render() {
		return (
			<div className="col-md-8 order-md-1">
				{/* <h4 className="mb-3">Ingreso Gasto</h4> */}
				<h4 className="d-flex justify-content-between align-items-center mb-3">
					<span className="text-muted">Ingreso Gasto</span>
				</h4>
				<form>
					<div className="form-row align-items-center">
						<div className="col-auto">
							<label className="sr-only" htmlFor="inlineFormInputGroup">
								Username
							</label>
							<div className="input-group mb-2">
								<div className="input-group-prepend">
									<div className="input-group-text bg-purple text-white lh-100">$ USD</div>
								</div>
								<input
									onChange={this.capturarGasto}
									type="number"
									className="form-control"
									id="inlineFormInputGroup"
									placeholder="Valor Gasto"
								/>
							</div>
						</div>
						<div className="col-auto">
							<button
								onClick={this.enviarGasto}
								type="button"
								className="btn bg-purple text-white btn-sm mb-2"
							>
								Agregar
							</button>
						</div>
					</div>
				</form>
			</div>
		);
	}
}
