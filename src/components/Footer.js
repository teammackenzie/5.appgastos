import React from 'react';

export default function Footer(props) {
	const { company } = props;
	return (
		<footer className="my-5 pt-5 text-muted text-center text-small">
			<p className="mb-1">&copy; 2019 {company}</p>
		</footer>
	);
}
