import React from 'react';
import './Gasto.css';
import { NumberFormat } from '../Helper';

export default function Gasto({ gasto }) {
	const colorGasto = gasto < 0 ? 'bg-entrada' : 'bg-salida';
	return (
		<li className={`list-group-item d-flex justify-content-between lh-condensed ${colorGasto}`}>
			<div>
				<h6 className="my-0">{gasto < 0 ? 'Entrada' : 'Salida'}</h6>
			</div>
			<span className="text-muted">{NumberFormat(gasto)}</span>
		</li>
	);
}
